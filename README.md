# Requests lxml: Responses wrapped with lxml #

Often I find myself making a request with `Requests` then using `lxml` with the
response, so I figured automating the process would save me some time.

    >>> import requests_lxml
    >>> resp = requests_lxml.get('https://www.google.com')
    >>> resp.xpath('//div')
    [<Element div at 0x1073aa410>,
     ...
     <Element div at 0x1073aa4b0>]
    >>> resp.xpath('//a/@href')
    [u'http://www.google.com/support/websearch/bin/answer.py?answer=186645&form=bb&hl=en-GB',
     ...
     u'http://www.google.com/intl/en/about.html']

By using requests_lxml.get (post, put etc) the response object has additional methods to assist getting the required information. Both XML and HTML parsers can be used (see `set_parser()`).

Lots of the functionality was inspired by mattseh's [magicrequests](https://github.com/mattseh/magicrequests]).

# Install #

    pip install https://bitbucket.org/johnsmith/requests_lxml/get/master.tar.gz

# Additional response methods #
## xpath shortcuts ##
These methods are all xpath related, many are shortcuts for common xpath
expressions. Where a method returns paths to resources, `urlparse` is used to
return the full links.

### response.xpath(expression) ###
Returns the result of the xpath expression (getting full urls via `urlparse` for
expressions that end with 'href' or 'src').

### response.xpath_multiple(expressions) ###
Returns the results of multiple xpath expressions.

If `expressions` is a list or tuple of xpath expressions, returns list of
tuples eg

    expression = ["//a/text()", "//a/@href"]

returns

    [('link text 1', 'url 1'), ('link text 2' , 'url 2')]

If `expressions` is a dict returns a list of dicts eg

    expression = {'text': "//a/text()", 'url': "//a/@href"}

returns

    [{'text': 'link text 1', 'url': 'url 1'}, {'text': 'link text 2', 'url': 'url 2'}]

## Convenience methods ##
### response.internal_links() ###
Returns the set of all internal links in the page.

### response.external_links() ###
Returns the set of all external links in the page (other subdomains count as
external).

### response.dofollow_links() ###
Returns the set of all dofollow links in the page.

### response.external_dofollow_links() ###
Returns the set of external dofollow links in the page.

### response.nofollow_links() ###
Returns the set of all nofollow links in the page.

### response.external_nofollow_links() ###
Returns the set of external nofollow links in the page.

### response.external_images() ###
Returns the set of external image urls in the page.

## Other methods ##
### response.save(handle, force_unicode=False, close=True) ###
Save the response content to `handle`, a file-like object.

## Parser selection ##
For html `lxml.etree.HTML` is used (the default), for xml `lxml.etree.fromstring` is used. When the parser is changed you have to make a new request in order to use it.

### set_parser(parser='html') ###
`'html'` and `'xml'` are the only valid arguments, anything else will raise an exception.
