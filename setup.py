#!/usr/bin/python
from setuptools import setup

with open('README.md') as file:
    long_description = file.read()

setup(name="requests_lxml",
        version = '0.4.1',
        description = "Python Requests with lxml etree functionality added to response objects",
        long_description = long_description,
        author = "johnsmith (Bitbucket)",
        license = "BSD licence",
        py_modules = ['requests_lxml'],
        requires = ['requests', 'lxml'],
)
