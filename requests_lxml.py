try:
    from urlparse import urlparse, urljoin
except ImportError:
    from urllib.parse import urlparse, urljoin
from lxml import etree, html
import requests


def xpath_multiple(self, expressions):
    """
    Returns the results of the xpath expressions.

    If `expressions` is a list or tuple of xpath expressions, returns list of tuples eg
    expression = ["//a/text()", "//a/@href"]
    returns [('link text 1', 'url 1'), ('link text 2' , 'url 2')]

    If `expressions` is a dict returns a list of dicts eg
    expression = {'text': "//a/text()", 'url': "//a/@href"}
    returns [{'text': 'link text 1', 'url': 'url 1'}, {'text': 'link text 2' , 'url': 'url 2'}]
    """
    results = []
    try: # dict
        xpath_results = []
        # get data
        for xpath in expressions.itervalues():
            xpath_results.append(self.xpath(xpath))

        # format output
        for item in zip(*xpath_results):
            d = {}
            for k, v in zip(expressions.keys(), item):
                d[k] = v
            results.append(d)

    except AttributeError: # list, tuple
        for item in expressions:
            results.append(self.xpath(item))
        results = zip(*results)

    return results


class _EtreeResponse(object):
    xpath_multiple = xpath_multiple

    patch_members = [
        '__contains__',
        'domain',
        'save',
        '_get_etree',
        'xpath',
        'xpath_multiple',
        'internal_links',
        'external_links',
        'dofollow_links',
        'external_dofollow_links',
        'nofollow_links',
        'external_nofollow_links',
        'external_images',
    ]

    @classmethod
    def patch_members_onto_other_class(cls, target):
        for name in cls.patch_members:
            setattr(target, name, getattr(cls, name))

    @classmethod
    def patch_member_onto_other_class(cls, name, target):
        if name not in cls.patch_members:
            raise Exception("Not a patchable member: {}".format(name))
        setattr(target, name, getattr(cls, name))

    def __contains__(self, item):
        return item.lower() in self.text.lower()

    @property
    def domain(self):
        if not hasattr(self, '_domain'):
            self._domain = urlparse(self.url).netloc
        return self._domain

    def save(self, handle, close=True):
        """
        Write content to `handle`.
        """
        handle.write(self.text)

        if close:
            handle.close()

    def _get_etree(self):
        raise NotImplementedError

    def xpath(self, expression):
        """
        Returns the result of the xpath expression (getting full urls via urlparse for expressions that end with 'href' or 'src').
        """
        if not hasattr(self, '_etree'):
            self._get_etree()

        results = []
        try:
            for result in self._etree.xpath(expression):
                if expression.endswith('href') or expression.endswith('src') and not result.startswith('http'):
                    result = urljoin(self.url, result)
                results.append(result)
        except TypeError: # eg count(//a) returns a float
            return self._etree.xpath(expression)

        return results

    def internal_links(self):
        """
        Returns the set of all internal links in the page.
        """
        return set([link for link in self.xpath('//a/@href') if urlparse(link).netloc == self.domain])

    def external_links(self):
        """
        Returns the set of all external links in the page (other subdomains count as external).
        """
        return set([link for link in self.xpath('//a/@href') if urlparse(link).netloc != self.domain])

    def dofollow_links(self):
        """
        Returns the set of all dofollow links in the page.
        """
        return set(self.xpath('//a[@rel!="nofollow" or not(@rel)]/@href'))

    def external_dofollow_links(self):
        """
        Returns the set of external dofollow links in the page.
        """
        external = self.external_links()
        dofollow = self.dofollow_links()
        return external & dofollow

    def nofollow_links(self):
        """
        Returns the set of all nofollow links in the page.
        """
        return set(self.xpath('//a[@rel="nofollow"]'))

    def external_nofollow_links(self):
        """
        Returns the set of external nofollow links in the page.
        """
        external = self.external_links()
        nofollow = self.nofollow_links()
        return external & nofollow

    def external_images(self):
        """
        Returns the set of external image urls in the page.
        """
        return set([image for image in self.xpath('//img/@src') if urlparse(image).netloc != self.domain])


class _HtmlEtreeResponse(_EtreeResponse):
    """
    Features the lxml.etree.HTML parser.
    """
    def _get_etree(self):
        self._etree = etree.HTML(self.content)


class _FragmentHtmlEtreeResponse(_EtreeResponse):
    """
    Features the lxml.etree.HTML parser.
    """
    def _get_etree(self):
        self._etree = html.fragment_fromstring(self.content, create_parent='div')


class _XmlEtreeResponse(_EtreeResponse):
    """
    Features the lxml.etree.fromstring parser.
    """
    def _get_etree(self):
        self._etree = etree.fromstring(self.content)


# add methods from _HtmlEtreeResponse to the Response model (default to html parser)
_HtmlEtreeResponse.patch_members_onto_other_class(requests.models.Response)


def set_parser(parser='html'):
    """
    Set the lxml parser to be used, `html` uses `lxml.etree.HTML` and `xml` uses `lxml.etree.fronstring`.
    """
    if parser == 'html':
        _HtmlEtreeResponse.patch_member_onto_other_class('_get_etree', requests.models.Response)

    elif parser == 'xml':
        _XmlEtreeResponse.patch_member_onto_other_class('_get_etree', requests.models.Response)

    elif parser == 'fragment':
        _FragmentHtmlEtreeResponse.patch_member_onto_other_class('_get_etree', requests.models.Response)

    else:
        raise Exception("Invalid parser (%s), s/be 'html', 'xml' or 'fragment'")
